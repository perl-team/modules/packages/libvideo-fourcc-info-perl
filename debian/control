Source: libvideo-fourcc-info-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Dominic Hargreaves <dom@earth.li>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: libdatetime-perl,
                     libdbd-sqlite3-perl,
                     libdbi-perl,
                     libtest-nowarnings-perl,
                     libtest-without-module-perl,
                     perl
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libvideo-fourcc-info-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libvideo-fourcc-info-perl.git
Homepage: https://metacpan.org/release/Video-FourCC-Info

Package: libvideo-fourcc-info-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libdatetime-perl,
         libdbd-sqlite3-perl,
         libdbi-perl
Description: Perl module to find information about codecs from its FourCC
 Video::FourCC::Info finds information about a given Four Character Code
 (FourCC).
 .
 This code is a four-byte sequence embedded in video files that tells video
 players what algorithm is required to decode a given video stream. This
 detection scheme ensures that the detected codec format is independent of
 the file extension, which may be incorrect due to human error or for some
 other reason.
